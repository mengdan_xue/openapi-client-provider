package com.acooly.module.openapi.client.provider.fudian.exception;

/**
 * @author zhike 2018/3/15 14:28
 */
public class FudianProcessingException extends RuntimeException{

    public FudianProcessingException() {
    }

    public FudianProcessingException(String message, Throwable cause) {
        super(message, cause);
    }

    public FudianProcessingException(String message) {
        super(message);
    }

    public FudianProcessingException(Throwable cause) {
        super(cause);
    }
}
