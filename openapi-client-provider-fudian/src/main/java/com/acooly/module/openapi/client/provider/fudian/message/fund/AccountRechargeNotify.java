/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:14:52 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianNotify;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-22 04:14:52
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.ACCOUNT_RECHARGE ,type = ApiMessageType.Notify)
public class AccountRechargeNotify extends FudianNotify {

    /**
     * 账户号
     * 用户在本系统的唯一账户编号，由本系统生成.
     */
    @NotEmpty
    @Length(max=50)
    private String accountNo;

    /**
     * 充值金额
     * 充值金额，以元为单位，保留小数点后2位
     */
    @NotEmpty
    @Length(max=20)
    private String amount;

    /**
     * 充值手续费
     * 充值手续费，默认向用户收取以元为单位，保留小数点后2位
     */
    @NotEmpty
    @Length(max=20)
    private String fee;

    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    @NotEmpty
    @Length(max=8)
    private String merchantNo;

    /**
     * 支付方式
     * 支付方式 （个人、企业都可）1:快捷充值，3:网关充值，5:银行直连直充（限绑定富滇银行卡）
     */
    @NotEmpty
    @Length(min = 1,max=1)
    private String payType;

    /**
     * 到账金额
     * 实际到账的金额，以元为单位，保留小数点后2位
     */
    @NotEmpty
    @Length(max=20)
    private String receivedAmount;

    /**
     * 用户名
     * 用户在本系统的唯一标识，由本系统生成
     */
    @NotEmpty
    @Length(max=32)
    private String userName;

    /**
     * 状态
     * 充值状态  0：充值处理中 1充值成功 2充值失败 3状态不明
     */
    @NotEmpty
    @Length(min = 1,max=1)
    private String status;
}