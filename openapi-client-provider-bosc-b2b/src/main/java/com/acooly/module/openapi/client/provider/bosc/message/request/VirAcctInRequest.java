package com.acooly.module.openapi.client.provider.bosc.message.request;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotBlank;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequestDomain;

@Getter
@Setter
public class VirAcctInRequest extends BoscRequestDomain {

	/**
	 * 虚账户
	 */
	@NotBlank
	private String eAcctNo;

	/**
	 * 代发账户
	 */
	@NotBlank
	private String payAccount;

	/**
	 * 代发账户名
	 */
	@NotBlank
	private String payAccountName;

	@MoneyConstraint(min = 1L)
	private Money amount;

	/**
	 * 调用方流水号
	 */
	@NotBlank
	private String channelFlowNo;

}
