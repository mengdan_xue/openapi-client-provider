/**
 * create by zhangpu
 * date:2015年3月2日
 */
package com.acooly.module.openapi.client.provider.cj;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.AbstractApiServiceClient;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.exception.ApiServerException;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.cj.domain.CjNotify;
import com.acooly.module.openapi.client.provider.cj.domain.CjRequest;
import com.acooly.module.openapi.client.provider.cj.domain.CjResponse;
import com.acooly.module.openapi.client.provider.cj.marshall.CjNotifyUnmarshall;
import com.acooly.module.openapi.client.provider.cj.marshall.CjRedirectMarshall;
import com.acooly.module.openapi.client.provider.cj.marshall.CjRedirectPostMarshall;
import com.acooly.module.openapi.client.provider.cj.marshall.CjRequestMarshall;
import com.acooly.module.openapi.client.provider.cj.marshall.CjResponseUnmarshall;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;

/**
 * 上海银行P2P存管ApiService执行器  外部业务禁止使用
 *
 * @author zhangpu
 */
@Slf4j
@Component("cjApiServiceClient")
public class CjApiServiceClient
        extends AbstractApiServiceClient<CjRequest, CjResponse, CjNotify, CjNotify> {

    public static final String PROVIDER_NAME = "cj";

    @Resource(name = "cjHttpTransport")
    private Transport transport;
    @Resource(name = "cjRequestMarshall")
    private CjRequestMarshall requestMarshal;
    @Resource(name = "cjRedirectMarshall")
    private CjRedirectMarshall redirectMarshal;
    @Resource(name = "cjRedirectPostMarshall")
    private CjRedirectPostMarshall cjRedirectPostMarshall;

    @Autowired
    private CjResponseUnmarshall responseUnmarshal;

    @Autowired
    private CjNotifyUnmarshall notifyUnmarshal;

    @Autowired
    private CjProperties cjProperties;

    @Override
    public CjResponse execute(CjRequest request) {
        try {
            beforeExecute(request);
            if(Strings.isBlank(request.getGatewayUrl())) {
                request.setGatewayUrl(cjProperties.getGatewayUrl());
            }
            String url = request.getGatewayUrl();
            String requestMessage = getRequestMarshal().marshal(request);
            log.info("请求报文: {}", requestMessage);
            String responseMessage = getTransport().exchange(requestMessage, url);
            CjResponse t = getResponseUnmarshal().unmarshal(responseMessage, request.getService());
            afterExecute(t);
            return t;
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    @Override
    public PostRedirect redirectPost(CjRequest request) {
        try {
            beforeExecute(request);
            PostRedirect postRedirect = cjRedirectPostMarshall.marshal(request);
            return postRedirect;
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    @Override
    protected ApiMarshal<String, CjRequest> getRequestMarshal() {
        return this.requestMarshal;
    }

    @Override
    protected ApiUnmarshal<CjResponse, String> getResponseUnmarshal() {
        return this.responseUnmarshal;
    }

    @Override
    protected ApiUnmarshal<CjNotify, Map<String, String>> getNoticeUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    protected ApiMarshal<String, CjRequest> getRedirectMarshal() {
        return this.redirectMarshal;
    }

    @Override
    protected Transport getTransport() {
        return this.transport;
    }

    @Override
    protected ApiUnmarshal<CjNotify, Map<String, String>> getReturnUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    public String getName() {
        return PROVIDER_NAME;
    }
}
