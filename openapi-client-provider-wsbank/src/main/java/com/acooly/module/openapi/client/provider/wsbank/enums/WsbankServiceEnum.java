/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by ouwen
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.wsbank.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhike
 * Date: 2017-03-30 15:12:06
 */
public enum WsbankServiceEnum implements Messageable {
    UNKNOWN("UNKNOWN","UNKNOWN","未知"),
    SEND_SMS_CODE("sendSmsCode","ant.mybank.merchantprod.sendsmscode","短息验证码发送"),
    UPLOAD_IMG("uploadImg","ant.mybank.merchantprod.merchant.uploadphoto","图片上传"),
    TRADE_ORDER_QUERY("tradeOrderQuery","ant.mybank.bkmerchanttrade.payQuery", "订单查询"),
    PRE_PAY_NOTICE("prePayNotice","ant.mybank.bkmerchanttrade.prePayNotice", "线下支付结果异步通知"),
    ONLINE_PAY_NOTICE("onlinePayNotice","ant.mybank.bkmerchanttrade.onlinePayNotice", "支付宝APP/WAP支付结果通知"),
    VOSTRO_NOTIFY("vostroNotify","ant.mybank.bkcloudfunds.vostro.notify", "来帐汇入通知"),
    ORDERSHARE_NOTIFY("ordershareNotify","ant.mybank.bkcloudfunds.ordershare.notify", "分账通知"),
    WITHDRAW_NOTIFY("withdrawNotify","ant.mybank.bkcloudfunds.withdraw.notify", "提现结果通知"),

    
    MERCHANT_UNIFIED_RGISTER("merchantUnifiedRgister","ant.mybank.merchantprod.merchant.unified.register", "商户统一进件接口"),
    BKMERCHANTTRADE_DYNAMICORDER("bkmerchanttradedynamicOrder","ant.mybank.bkmerchanttrade.dynamicOrder", "动态订单扫码支付接口"),
    ALIPAY_REQUEST("alipayrequest","ant.mybank.bkmerchanttrade.onlinepay.alipay.request", "支付宝APP/WAP支付接口"),
    BKMERCHANTTRADE_PAYCLOSE("bkmerchanttradePayClose","ant.mybank.bkmerchanttrade.payClose", "订单关闭接口"),
    BKMERCHANTTRADE_PAYCANCEL("bkmerchanttradePayCancel","ant.mybank.bkmerchanttrade.payCancel", "订单撤销接口"),
    BKMERCHANTTRADE_REFUND("bkmerchanttradeRefund","ant.mybank.bkmerchanttrade.refund", "退款接口"),
    BKMERCHANTTRADE_REFUND_QUERY("bkmerchanttradeRefundQuery","ant.mybank.bkmerchanttrade.refundQuery", "退款查询"),


    REGISTER_QUERY("registerQuery","ant.mybank.merchantprod.merchant.register.query","入驻结果查询"),
    ADD_MERCHANT_CONFIG("addMerchantConfig","ant.mybank.merchantprod.merchant.addMerchantConfig","微信子商户支付配置接口"),
    MERCHANT_QUERY("merchantQuery","ant.mybank.merchantprod.merchant.query","商户信息查询接口"),
    MERCHANT_UPDATE("merchantUpdate","ant.mybank.merchantprod.merchant.unified.updateMerchant","统一进件商户信息修"),
    MERCHANT_FREEZE("merchantFreeze","ant.mybank.merchantprod.merchant.freeze","商户关闭"),
    MERCHANT_UNFREEZE("merchantUnFreeze","ant.mybank.merchantprod.merchant.unfreeze","商户开启"),
    BKMERCHANTTRADE_PAY("bkmerchanttradePay","ant.mybank.bkmerchanttrade.pay","移动刷卡支付（被扫）接口"),
    BKMERCHANTTRADE_PREPAY("bkmerchanttradePrePay","ant.mybank.bkmerchanttrade.prePay","H5支付（主扫）/公众号支付创建订单"),

    MERCHANT_OPEN_PAY("merchantOpenPay", "ant.mybank.bkcloudfunds.merchant.openPay", "开通商户余额支付能力"),
    VOSTRO_CREATE_ORDER("vostroCreateOrder", "ant.mybank.bkcloudfunds.vostro.createOrder", "来帐汇入订单创建"),
    VOSTRO_QUERY_ORDER("vostroQueryOrder", "ant.mybank.bkcloudfunds.vostro.queryorder", "单笔来帐汇入订单查询"),

    OPEN_PAY("openPay", "ant.mybank.bkcloudfunds.merchant.openPay", "开通商户余额支付能力"),
    BALANCE_PAY("balancePay", "ant.mybank.bkcloudfunds.balance.pay", "余额支付创建"),
    BALANCE_PAY_CONFIRM("balancePayConfirm", "ant.mybank.bkcloudfunds.balance.payconfirm", "余额支付确认"),
    BALANCE_PAY_QUERY("balancePayQuery", "ant.mybank.bkcloudfunds.balance.payquery", "余额支付查询"),
    ORDER_SHARE("orderShare", "ant.mybank.bkcloudfunds.order.share", "分账接口"),
    ORDER_SHARE_QUERY("orderShareQuery", "ant.mybank.bkcloudfunds.ordershare.query", "分账查询接口"),
    WITHDRAW_APPLY("withdrawApply", "ant.mybank.bkcloudfunds.withdraw.apply", "商户单笔提现申请接口"),
    WITHDRAW_APPLY_QUERY("withdrawApplyQuery", "ant.mybank.bkcloudfunds.withdraw.query", "商户单笔提现查询接口"),
    WITHDRAW_CONFIRM("withdrawConfirm", "ant.mybank.bkcloudfunds.withdraw.applyconfirm", "商户单笔提现确认接口")
    ;

    private final String code;
    private final String key;
    private final String message;

    WsbankServiceEnum(String code, String key, String message) {
        this.code = code;
        this.message = message;
        this.key = key;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public String getKey() {
        return key;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (WsbankServiceEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static WsbankServiceEnum find(String code) {
        for (WsbankServiceEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param key 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 key 没有对应的 Status 。
     */
    public static WsbankServiceEnum findByKey(String key) {
        for (WsbankServiceEnum status : values()) {
            if (status.getKey().equals(key)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<WsbankServiceEnum> getAll() {
        List<WsbankServiceEnum> list = new ArrayList<WsbankServiceEnum>();
        for (WsbankServiceEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (WsbankServiceEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
