package com.acooly.module.openapi.client.provider.yinsheng.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengNotify;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.NETBANK_DEPOSIT, type = ApiMessageType.Notify)
public class YinShengEbankDepositNotify extends YinShengNotify {

    /**
     * 商户订单号
     */
    private String out_trade_no;

    /**
     * 银盛流水号
     */
    private String trade_no;

    /**
     * 状态
     */
    private String trade_status;

    /**
     * 订单总金额
     */
    private String total_amount;

    /**
     * 入账的时间
     */
    private String account_date;
}
