package com.acooly.module.openapi.client.provider.yinsheng.message.netbank;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengNotify;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/1/9 10:10
 */
@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.APP_PAY, type = ApiMessageType.Notify)
public class YinShengAppPayNotify extends YinShengNotify{

    /**
     * 商户订单号
     * 商户请求的订单号.
     */
    private String out_trade_no;

    /**
     * 订单总金额
     * 该笔订单的资金总额单位为RMB-Yuan.取值范围为[0.01, 100000000.00],精确到小数点后两位
     */
    private String total_amount;

    /**
     * 银盛流水号
     * 该交易在银盛支付系统中的交易流水号
     */
    private String trade_no;

    /**
     * 状态
     * 	交易目前所处的状态。成功状态的值： TRADE_SUCCESS|TRADE_CLOSED等具体详情看下文中的交易状态详解
     */
    private String trade_status;

    /**
     * 入账的时间
     * 入账的时间，格式"yyyy-MM-dd"
     */
    private String account_date;
}
