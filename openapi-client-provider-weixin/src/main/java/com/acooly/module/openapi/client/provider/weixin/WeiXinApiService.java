package com.acooly.module.openapi.client.provider.weixin;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.binarywang.wxpay.service.WxPayService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class WeixinApiService {

	@Resource(name = "wxPayService")
	private WxPayService client;
	@Resource(name = "weixinApiServiceClient")
	private WeixinApiServiceClient apiServiceClient;
	@Autowired
	private OpenAPIClientWeixinProperties openAPIClientWeixinProperties;
}
