package com.acooly.module.openapi.client.provider.weixin.domain;

import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.provider.weixin.support.WeixinAlias;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WeixinApiMessage implements ApiMessage {

	/** 接口名称 */
	private String service;
	
	@WeixinAlias(value = "app_id")
	private String appId;

	@Override
	public String toString() {
		return ToString.toString(this);
	}

	@Override
	public String getService() {
		return this.service;
	}

	@Override
	public String getPartner() {
		return this.appId;
	}
}
