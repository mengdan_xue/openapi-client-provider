/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月5日
 *
 */
package com.acooly.module.openapi.client.provider.fuiou.message;

import com.acooly.module.openapi.client.provider.fuiou.domain.FuiouResponse;

/**
 * 单独冻结 响应报文
 * 
 * @author liuyuxiang
 */
public class FreezeResponse extends FuiouResponse {
	
}
