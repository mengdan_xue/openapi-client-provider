/**
 * create by zhangpu
 * date:2015年3月24日
 */
package com.acooly.module.openapi.client.provider.fuiou.enums;

/**
 * @author zhangpu
 *
 */
public enum FuiouBusiTpEnums {
	PWPC("PWPC ", "转账"),
	
	PW13("PW13", "预授权"),

	PWCF("PWCF", "预授权撤销"),
	
	PW03("PW03", "划拨"),
	
	PW14("PW14","转账冻结"),
	
	PW15("PW15", "划拨冻结"),
	
	PWDJ("PWDJ", "冻结"),
	
	PWJD("PWJD","解冻"),
	
	PW19("PW19","冻结付款到冻结");
	

	private String key;
	private String val;

	private FuiouBusiTpEnums(String key, String val) {
		this.key = key;
		this.val = val;
	}

	public String getKey() {
		return key;
	}

	public String getVal() {
		return val;
	}

}
