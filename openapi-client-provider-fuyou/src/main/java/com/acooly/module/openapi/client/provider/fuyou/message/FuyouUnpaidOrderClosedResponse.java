package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouResponse;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:51
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.UNPAID_ORDER_CLOSED,type = ApiMessageType.Response)
@XStreamAlias("RESPONSE")
public class FuyouUnpaidOrderClosedResponse extends FuyouResponse{

    /**
     * 商户订单号
     * 商户订单流水号商户确保唯一
     */
    @Size(max = 60)
    @NotBlank
    @XStreamAlias("MCHNTORDERID")
    private String merchOrderNo;


    /**
     * 富友订单号
     * 富友生成的订单号，该订单号在相
     当长的时间内不重复。富友通过订
     单号来唯一确认一笔订单的重复性
     */
    @XStreamAlias("ORDERID")
    private String bankOrderId;

    /**
     * 交易金额
     * 交易金额，分为单位
     */
    @XStreamAlias("AMT")
    @NotBlank
    @Size(max = 12)
    private String amount;

    /**
     * 签名类型
     */
    @XStreamAlias("SIGNTP")
    private String signType;

    /**
     * 摘要数据
     */
    @XStreamAlias("SIGN")
    private String sign;

    @Override
    public String getSignStr() {
        return getResponseCode()+"|"+getPartner()+"|"+getMerchOrderNo()+"|"+getBankOrderId()+"|"+getAmount()+"|";
    }
}
