package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouRequest;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:04
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.MERSUPPORT_CARDBIN_QUERY,type = ApiMessageType.Request)
@XStreamAlias("FM")
public class FuyouMerSupportCardBinQueryRequest extends FuyouRequest{

    /**
     * 商户代码
     * 富友分配给各合作商户的唯一识别码
     */
    @XStreamAlias("MchntCd")
    @NotBlank
    private String mchntCd;

    /**
     * 银行卡号
     * 支付的银行卡卡号
     */
    @Size(max = 60)
    @NotBlank
    @XStreamAlias("Ono")
    private String oNo;

    public void setMchntCd(String mchntCd) {
        this.mchntCd = getPartner();
    }

    @Override
    public String getSignStr() {
        return getMchntCd()+"|"+getONo()+"|";
    }
}
