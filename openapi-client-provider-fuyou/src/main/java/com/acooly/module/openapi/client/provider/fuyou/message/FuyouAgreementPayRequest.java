package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouRequest;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:04
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.AGREEMENT_PAY,type = ApiMessageType.Request)
@XStreamAlias("REQUEST")
public class FuyouAgreementPayRequest extends FuyouRequest{

    /**
     * 客户 IP
     * 客户所在 IP 地址
     */
    @XStreamAlias("USERIP")
    @NotBlank
    private String userIp;

    /**
     * 交易类型
     */
    @XStreamAlias("TYPE")
    @NotBlank
    private String type = "03";

    /**
     * 商户订单号
     * 商户订单流水号商户确保唯一
     */
    @Size(max = 60)
    @NotBlank
    @XStreamAlias("MCHNTORDERID")
    private String merchOrderNo;

    /**
     * 用户编号
     * 商户端用户的唯一编号，即用户 ID
     */
    @XStreamAlias("USERID")
    @NotBlank
    @Size(max = 40)
    private String userId;

    /**
     * 富友订单号
     * 富友生成的订单号，该订单号在相
     当长的时间内不重复。富友通过订
     单号来唯一确认一笔订单的重复性
     */
    @XStreamAlias("ORDERID")
    @Size(max = 40)
    private String bankOrderId;

    /**
     * 协议号
     * 首次协议交易成功好生成的协议号
     */
    @XStreamAlias("PROTOCOLNO")
    @NotBlank
    @Size(max = 30)
    private String protocolNo;

    /**
     * 短信验证码
     * 用户获取到的富友发送的手机短信
     * 验证码
     */
    @XStreamAlias("VERCD")
    @NotBlank
    private String vercd;


    /**
     * 保留字段 1
     */
    @XStreamAlias("REM1")
    @NotBlank
    @Size(max = 256)
    private String remOne;

    /**
     * 保留字段 2
     */
    @XStreamAlias("REM2")
    @NotBlank
    @Size(max = 256)
    private String remTwo;

    /**
     * 保留字段 3
     */
    @XStreamAlias("REM3")
    @NotBlank
    @Size(max = 256)
    private String remThree;

    /**
     * 下单验证码
     */
    @XStreamAlias("SIGNPAY")
    @NotBlank
    private String signPay;

    @Override
    public String getSignStr() {
        return getType()+"|"+getVersion()+"|"+getPartner()+"|"+getBankOrderId()+"|"+getMerchOrderNo()+"|"+getProtocolNo()+"|"+getUserId()+"|"+getVercd()+"|";
    }
}
