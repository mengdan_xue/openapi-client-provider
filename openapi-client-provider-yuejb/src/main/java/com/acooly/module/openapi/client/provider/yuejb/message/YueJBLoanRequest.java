package com.acooly.module.openapi.client.provider.yuejb.message;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.yuejb.domain.YueJBRequest;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by ouwen@yiji.com} on 2017/11/6.
 */
@Data
public class YueJBLoanRequest extends YueJBRequest {
    /**
     * 额度账单金额
     */
    @MoneyConstraint(min = 1L)
    private Money creditAmount;

    /**
     * 额度系统冻结流水号
     */
    @NotEmpty
    private String freezeQuotaBizId;

    /**
     * 放款金额
     */
    @MoneyConstraint(min = 1L)
    private Money loanAmount;

    /**
     * 产品编号
     */
    @NotEmpty
    private String productNo;

    /**
     * 	账户种类 B:对公银行卡号,C:对私银行卡号
     */
    private String receiptAccountClass;
    /**
     * 	收款账户名称 B:对公银行卡号,C:对私银行卡号
     */
    private String receiptAccountName;
    /**
     * 	收款账户号
     */
    @NotEmpty
    private String receiptAccountNo;
    /**
     * 	收款账户类型
     */
    @NotEmpty
    private String receiptAccountType;
    /**
     * 	银行编码
     */
    private String receiptBankCode;
    /**
     * 标识码
     */
    @NotEmpty
    private String markCode;
    /**
     * 是否解冻全部冻结金额
     */
    private String unfreezeAll;
}
