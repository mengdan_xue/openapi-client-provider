package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscBizTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.message.fund.info.BizDetailInfo;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.util.Assert;

import java.util.List;

/**
 * Created by liubin@prosysoft.com on 2017/10/10.
 */
@ApiMsgInfo(service = BoscServiceNameEnum.ASYNC_TRANSACTION, type = ApiMessageType.Request)
public class AsyncTransactionRequest extends BoscRequest {
	
	@NotBlank(message = "批次号不能为空")
	private String batchNo;
	
	@NotEmpty(message = "交易明细不能为空")
	private List<BizDetailInfo> bizDetails;
	
	public AsyncTransactionRequest () {
		setService (BoscServiceNameEnum.ASYNC_TRANSACTION.code ());
	}
	
	public AsyncTransactionRequest (String batchNo,
	                                List<BizDetailInfo> bizDetails) {
		this();
		this.batchNo = batchNo;
		this.bizDetails = bizDetails;
	}
	
	public String getBatchNo () {
		return batchNo;
	}
	
	public void setBatchNo (String batchNo) {
		this.batchNo = batchNo;
	}
	
	public List<BizDetailInfo> getBizDetails () {
		return bizDetails;
	}
	
	public void setBizDetails (
			List<BizDetailInfo> bizDetails) {
		this.bizDetails = bizDetails;
	}
	
	@Override
	public void doCheck () {
		super.doCheck ();
		bizDetails.stream ().forEach (bizDetail -> {
			bizDetail.getDetails ().stream ().forEach (detail ->{
				if(BoscBizTypeEnum.TENDER.equals (detail.getBizType ())){
					Assert.hasText (detail.getFreezeRequestNo (),"业务类型为投标时,投标预处理流水号不能为空");
				}
			});
		});
		
	}
}
