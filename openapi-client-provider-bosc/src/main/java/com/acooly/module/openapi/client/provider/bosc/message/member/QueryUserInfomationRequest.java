/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-22 13:28 创建
 */
package com.acooly.module.openapi.client.provider.bosc.message.member;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhangpu 2017-09-22 13:28
 */
@ApiMsgInfo(service = BoscServiceNameEnum.QUERY_USER_INFORMATION,type = ApiMessageType.Request)
public class QueryUserInfomationRequest extends BoscRequest {

    @NotEmpty
    @Size(max = 50)
    private String platformUserNo;

    public String getPlatformUserNo() {
        return platformUserNo;
    }

    public void setPlatformUserNo(String platformUserNo) {
        this.platformUserNo = platformUserNo;
    }


    public QueryUserInfomationRequest() {
        setService(BoscServiceNameEnum.QUERY_USER_INFORMATION.code());
    }
}
