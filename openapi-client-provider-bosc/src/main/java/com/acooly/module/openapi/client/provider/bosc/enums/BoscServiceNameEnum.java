/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 19:29 创建
 */
package com.acooly.module.openapi.client.provider.bosc.enums;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * BOSC服务名称枚举
 *
 * @author zhangpu 2017-09-24 19:29
 */
public enum BoscServiceNameEnum implements Messageable {

    QUERY_USER_INFORMATION("QUERY_USER_INFORMATION", "用户信息查询"),

    PERSONAL_REGISTER_EXPAND("PERSONAL_REGISTER_EXPAND", "个人绑卡注册"),

    ENTERPRISE_REGISTER("ENTERPRISE_REGISTER", "企业绑卡注册"),
    
    UNBIND_BANKCARD("UNBIND_BANKCARD", "解绑银行卡"),

    WITHDRAW("WITHDRAW", "提现"),
    
    USER_AUTO_PRE_TRANSACTION("USER_AUTO_PRE_TRANSACTION","授权预处理"),
    
    CANCEL_PRE_TRANSACTION("CANCEL_PRE_TRANSACTION","取消预处理"),
    
    BACKROLL_RECHARGE("BACKROLL_RECHARGE","资金回退充值"),
    
    CONFIRM_WITHDRAW("CONFIRM_WITHDRAW","提现确认"),
    
    CANCEL_WITHDRAW("CANCEL_WITHDRAW","取消提现"),
    
    INTERCEPT_WITHDRAW("INTERCEPT_WITHDRAW","提现拦截"),
    
    RECHARGE("RECHARGE","充值"),
    
    ASYNC_TRANSACTION("ASYNC_TRANSACTION","批量交易"),
    
    ESTABLISH_PROJECT("ESTABLISH_PROJECT","创建标的"),
    
    MODIFY_PROJECT("MODIFY_PROJECT","标的状态修改"),
    
    QUERY_PROJECT_INFORMATION("QUERY_PROJECT_INFORMATION","查询标的信息"),
    
    AUTHORIZATION_ENTRUST_PAY("AUTHORIZATION_ENTRUST_PAY","委托支付授权"),
    
    QUERY_AUTHORIZATION_ENTRUST_PAY_RECORD("QUERY_AUTHORIZATION_ENTRUST_PAY_RECORD","委托支付授权记录查询"),
    
    DEBENTURE_SALE("DEBENTURE_SALE","债权转让"),
    
    CANCEL_DEBENTURE_SALE("CANCEL_DEBENTURE_SALE","取消债转"),
    
    FREEZE("FREEZE","冻结"),
    UNFREEZE("UNFREEZE","解冻"),
    RESET_PASSWORD("RESET_PASSWORD","找回密码"),
    MODIFY_MOBILE_EXPAND("MODIFY_MOBILE_EXPAND","预留手机号更新"),
    PERSONAL_BIND_BANKCARD_EXPAND("PERSONAL_BIND_BANKCARD_EXPAND","个人换绑卡"),
    ENTERPRISE_BIND_BANKCARD("ENTERPRISE_BIND_BANKCARD","企业换绑卡"),
    ACTIVATE_STOCKED_USER("ACTIVATE_STOCKED_USER","会员激活"),
    DOWNLOAD_CHECKFILE("DOWNLOAD_CHECKFILE","下载对账文件"),
    CONFIRM_CHECKFILE ("CONFIRM_CHECKFILE","对账文件确认"),
    QUERY_TRANSACTION ("QUERY_TRANSACTION","查询单笔交易明细"),
    QUERY_TRANSACTION_RECHARGE ("QUERY_TRANSACTION_RECHARGE","查询充值明细"),
    QUERY_TRANSACTION_WITHDRAW ("QUERY_TRANSACTION_WITHDRAW","查询提现明细"),
    QUERY_TRANSACTION_PRETRANSACTION ("QUERY_TRANSACTION_PRETRANSACTION","查询预处理明细"),
    QUERY_TRANSACTION_TRANSACTION ("QUERY_TRANSACTION_TRANSACTION","查询交易确认明细"),
    QUERY_TRANSACTION_FREEZE ("QUERY_TRANSACTION_FREEZE","查询冻结明细"),
    QUERY_TRANSACTION_UNFREEZE ("QUERY_TRANSACTION_UNFREEZE","查询解冻明细"),
    QUERY_TRANSACTION_DEBENTURE_SALE ("QUERY_TRANSACTION_DEBENTURE_SALE","查询债权出让明细"),
    QUERY_TRANSACTION_CANCEL_PRETRANSACTION ("QUERY_TRANSACTION_CANCEL_PRETRANSACTION","查询取消预处理明细"),
    QUERY_TRANSACTION_INTERCEPT_WITHDRAW ("QUERY_TRANSACTION_INTERCEPT_WITHDRAW","查询提现拦截明细"),
    QUERY_TRANSACTION_UPDATE_BANKCARD_AUDIT ("QUERY_TRANSACTION_UPDATE_BANKCARD_AUDIT","查询换卡记录明细"),
    ;

    private final String code;
    private final String message;

    private BoscServiceNameEnum (String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (BoscServiceNameEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static BoscServiceNameEnum find(String code) {
        for (BoscServiceNameEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("BoscServiceNameEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<BoscServiceNameEnum> getAll() {
        List<BoscServiceNameEnum> list = new ArrayList<BoscServiceNameEnum>();
        for (BoscServiceNameEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (BoscServiceNameEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
