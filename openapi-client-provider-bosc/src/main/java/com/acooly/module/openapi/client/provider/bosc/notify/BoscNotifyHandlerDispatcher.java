/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-26 10:53 创建
 */
package com.acooly.module.openapi.client.provider.bosc.notify;

import com.acooly.module.openapi.client.api.ApiServiceClient;
import com.acooly.module.openapi.client.api.notify.AbstractSpringNotifyHandlerDispatcher;
import com.acooly.module.openapi.client.provider.bosc.BoscApiServiceClient;
import com.acooly.module.openapi.client.provider.bosc.BoscConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * BOSC 专用异步通知分发器
 *
 * @author zhangpu 2017-09-26 10:53
 */
@Component
public class BoscNotifyHandlerDispatcher extends AbstractSpringNotifyHandlerDispatcher {

    @Autowired
    private BoscApiServiceClient boscApiServiceClient;

    @Override
    protected String getServiceKey(String notifyUrl, Map<String, String> notifyData) {
        return notifyData.get(BoscConstants.SERVICE_NAME);
    }

    @Override
    protected ApiServiceClient getApiServiceClient() {
        return this.boscApiServiceClient;
    }
}
