/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.baofup.domain;

import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/** @author zhike */
@Getter
@Setter
public class BaoFuPRequest extends BaoFuPApiMessage {

    /**
     * 报文流水号
     */
    @NotBlank
    @Size(max = 32)
    @BaoFuPAlias(value = "msg_id")
    private String msgId;

    /**
     * 数字信封（请求不用传）
     * 格式：01|对称密钥，01代表AES
     * 加密方式：Base64转码后使用宝付的公钥加密
     */
    @Size(max = 512)
    @BaoFuPAlias(value = "dgtl_envlp")
    private String dgtlEnvlp;
}
