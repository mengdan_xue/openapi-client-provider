package com.acooly.module.openapi.client.provider.baofup.message;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPRequest;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceEnum;
import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/4/12 15:59
 */
@Getter
@Setter
@BaoFuPApiMsgInfo(service = BaoFuPServiceEnum.PROTOCOL_PAY_APPLY_BIND_CARD,type = ApiMessageType.Request)
public class BaoFuProtocolPayApplyBindCardRequest extends BaoFuPRequest {

    /**
     * 用户ID
     * 用户在商户平台唯一ID
     */
    @Size(max = 50)
    @BaoFuPAlias(value = "user_id")
    private String userId;

    /**
     * 卡类型
     * 101:借记卡;102:信用卡
     */
    @NotBlank
    @BaoFuPAlias(value = "card_type")
    private String cardType = "101";

    /**
     * 证件类型
     * 01:身份证 目前只支持
     */
    @NotBlank
    @BaoFuPAlias(value = "id_card_type")
    private String idCardType = "01";

    /**
     * 账户信息(不用传,分开传入一下字段组建会自动拼装)
     * 格式：银行卡号|持卡人姓名|证件号|手机号|银行卡安全码|银行卡有效期（注顺序不能变且如果没有值请保留|）
     * 加密方式：Base64转码后，使用数字信封指定的方式和密钥加密
     */
    @Size(max = 200)
    @BaoFuPAlias(value = "acc_info",isEncrypt = true)
    private String accInfo;

    /**
     * 银行卡号
     */
    @NotBlank
    private String bankCardNo;

    /**
     * 持卡人姓名
     */
    @NotBlank
    private String realName;

    /**
     * 证件号
     */
    @NotBlank
    private String idCardNo;

    /**
     * 手机号
     */
    @NotBlank
    private String mobileNo;

    /**
     * 银行卡安全码
     * 信用卡的时候传
     */
    private String cvs;

    /**
     * 银行卡有效期
     */
    private String validity;

    /**
     * 组装请求信息
     * @return
     */
    public String getAccInfo() {
        StringBuilder accInfo = new StringBuilder();
        accInfo.append(getBankCardNo()).append("|").append(getRealName()).append("|")
                .append(getIdCardNo()).append("|").append(getMobileNo()).append("|");
        if(Strings.isNotBlank(getCvs())) {
            accInfo.append(getCvs());
        }
        accInfo.append("|");
        if(Strings.isNotBlank(getValidity())) {
            accInfo.append(getValidity());
        }
        accInfo.append("|");
        return accInfo.toString();
    }
}
