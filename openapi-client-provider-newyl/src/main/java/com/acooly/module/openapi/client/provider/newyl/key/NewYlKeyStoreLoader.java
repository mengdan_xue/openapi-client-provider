package com.acooly.module.openapi.client.provider.newyl.key;

import com.acooly.module.openapi.client.provider.newyl.support.NewYlKeyStoreInfo;
import com.acooly.module.safety.key.KeyLoader;

/**
 * @author zhike 2018/2/6 13:58
 */
public interface NewYlKeyStoreLoader extends KeyLoader<NewYlKeyStoreInfo> {

}
