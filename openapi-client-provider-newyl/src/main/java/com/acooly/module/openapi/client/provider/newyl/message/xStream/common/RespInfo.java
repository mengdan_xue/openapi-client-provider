package com.acooly.module.openapi.client.provider.newyl.message.xStream.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

/**
 * @author fufeng 2018/1/26 15:26.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("INFO")
public class RespInfo {
    /**
     *交易代码
     */
    @XStreamAlias("TRX_CODE")
    private String trxCode;
    /**
     *版本
     */
    @XStreamAlias("VERSION")
    private String version;
    /**
     *数据格式
     */
    @XStreamAlias("DATA_TYPE")
    private String dataType;
    /**
     *交易流水号
     */
    @XStreamAlias("REQ_SN")
    private String reqSn;
    /**
     *返回代码
     */
    @XStreamAlias("RET_CODE")
    private String retCode;
    /**
     *处理级别
     */
    @XStreamAlias("LEVEL")
    private String level;
    /**
     *错误信息
     */
    @XStreamAlias("ERR_MSG")
    private String errMsg;
    /**
     *签名信息
     */
    @XStreamAlias("SIGNED_MSG")
    private String signedMsg;

}
