//package com.acooly.openapi.client.provider.jyt.file;
//
//import com.acooly.core.common.BootApp;
//import com.acooly.module.openapi.client.file.FileDataHandler;
//import com.acooly.module.openapi.client.file.domain.FileHandlerOrder;
//import com.acooly.module.openapi.client.provider.jyt.JytConstants;
//import com.acooly.module.openapi.client.provider.jyt.file.JytSftpFileHandler;
//import com.acooly.test.NoWebTestBase;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//
//import java.util.List;
//
//@Slf4j
//@SpringBootApplication
//@BootApp(sysName = "test")
//public class JytFileHandlerTest extends NoWebTestBase {
//
//    @Autowired
//    JytSftpFileHandler jytSftpFileHandler;
//
//
//    /**
//     * 硬编码方式处理文件
//     */
//    @Test
//    public void downloadHandleWithCodingTest() {
//
//        FileHandlerOrder order = new FileHandlerOrder("20180508", "JytConstants.PROVIDER_NAME");
//        order.put("fileName", "RNPAY_301060120002_20180508.txt");
//        jytSftpFileHandler.downloadHandle(order, new FileDataHandler() {
//            @Override
//            public void handle(String period, int index, List<String> rows, boolean finished) {
//                log.info("page {}, period: {}, finished:{}", index, period, finished);
//                for (String row : rows) {
//                    log.info(row);
//                }
//                log.info("");
//            }
//
//            @Override
//            public int getBatchSize() {
//                return 10;
//            }
//
//            @Override
//            public String name() {
//                return JytConstants.PROVIDER_NAME;
//            }
//        });
//    }
//
//
//    @Test
//    public void downloadHandleWithSpringTest() {
//
//        FileHandlerOrder order = new FileHandlerOrder("20180508", JytConstants.PROVIDER_NAME);
//        order.put("fileName", "RNPAY_301060120002_20180508");
//        jytSftpFileHandler.downloadHandle(order);
//    }
//
//
//}
