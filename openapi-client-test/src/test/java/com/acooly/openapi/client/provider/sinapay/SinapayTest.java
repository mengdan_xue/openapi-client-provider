package com.acooly.openapi.client.provider.sinapay;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Dates;
import com.acooly.core.utils.Ids;
import com.acooly.core.utils.Money;
import com.acooly.module.openapi.client.provider.sinapay.SinapayApiService;
import com.acooly.module.openapi.client.provider.sinapay.enums.*;
import com.acooly.module.openapi.client.provider.sinapay.message.member.*;
import com.acooly.module.openapi.client.provider.sinapay.message.trade.*;
import com.acooly.module.openapi.client.provider.sinapay.message.trade.dto.DebtChangeDetail;
import com.acooly.module.openapi.client.provider.sinapay.message.trade.dto.TradeOptInfo;
import com.acooly.module.openapi.client.provider.sinapay.message.trade.dto.TradePayInfo;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;
import java.util.List;

/**
 * @author zhike 2018/7/9 18:09
 */
@SpringBootApplication
@BootApp(sysName = "sinapayTest")
public class SinapayTest extends NoWebTestBase {

    @Autowired
    private SinapayApiService sinapayApiService;
    /**
     * 2.1	创建激活会员
     */
    @Test
    public void createActivateMember() {
        CreateActivateMemberRequest request = new CreateActivateMemberRequest();
        request.setIdentityId(Ids.oid());
        request.setMemberType("2");
        request.setClientIp("127.0.0.1");
        CreateActivateMemberResponse response = sinapayApiService.createActivateMember(request);
        System.out.println("创建激活会员接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.2	设置实名信息
     */
    @Test
    public void setRealName() {
        SetRealNameRequest request = new SetRealNameRequest();
        request.setIdentityId("o18070922054578600001");
        request.setCertNo("152922199408122910");
        request.setRealName("志客");
        request.setClientIp("127.0.0.1");
        SetRealNameResponse response = sinapayApiService.setRealName(request);
        System.out.println("设置实名信息接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.3	设置支付密码重定向
     */
    @Test
    public void setPayPassword() {
        SetPayPasswordRequest request = new SetPayPasswordRequest();
        request.setIdentityId("o18072122241147160001");
        request.setReturnUrl("https://www.baidu.com");
        SetPayPasswordResponse response = sinapayApiService.setPayPassword(request);
        System.out.println("设置支付密码重定向接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.4	修改支付密码重定向
     */
    @Test
    public void modifyPayPassword() {
        ModifyPayPasswordRequest request = new ModifyPayPasswordRequest();
        request.setIdentityId("o18070922054578600001");
        request.setReturnUrl("https://www.baidu.com");
        ModifyPayPasswordResponse response = sinapayApiService.modifyPayPassword(request);
        System.out.println("修改支付密码重定向接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.5	找回支付密码重定向
     */
    @Test
    public void findPayPassword() {
        FindPayPasswordRequest request = new FindPayPasswordRequest();
        request.setIdentityId("o18070922054578600001");
        request.setReturnUrl("https://www.baidu.com");
        FindPayPasswordResponse response = sinapayApiService.findPayPassword(request);
        System.out.println("找回支付密码重定向接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.6	查询是否设置支付密码
     */
    @Test
    public void querySetPayPassword() {
        QuerySetPayPasswordRequest request = new QuerySetPayPasswordRequest();
        request.setIdentityId("o18070922054578600001");
        QuerySetPayPasswordResponse response = sinapayApiService.querySetPayPassword(request);
        System.out.println("查询是否设置支付密码接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.7	绑定银行卡
     */
    @Test
    public void bindingBankCard() {
        BindingBankCardRequest request = new BindingBankCardRequest();
        request.setRequestNo(Ids.oid());
        request.setIdentityId("o18070922054578600001");
        request.setBankCode(SinapayBankCode.ABC.getCode());
        request.setBankAccountNo("6228481200290317811");
        request.setAccountName("志客");
        request.setCardType(SinapayCardType.DEBIT.getCode());
        request.setCardAttribute(SinapayCardAttribute.C.getCode());
        request.setCertType("IC");
        request.setCertNo("152922199408122910");
        request.setPhoneNo("18696725229");
        request.setProvince("重庆市");
        request.setCity("渝北区");
        request.setClientIp("127.0.0.1");
        request.setVerifyMode(VerifyModeEnum.SIGN.getCode());
        BindingBankCardResponse response = sinapayApiService.bindingBankCard(request);
        System.out.println("绑定银行卡接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.8	绑定银行卡推进
     */
    @Test
    public void bindingBankCardAdvance() {
        BindingBankCardAdvanceRequest request = new BindingBankCardAdvanceRequest();
        request.setTicket("4cf13184715e4b5f914a9300056b79ef");
        request.setValidCode("327216");
        request.setClientIp("127.0.0.1");
        BindingBankCardAdvanceResponse response = sinapayApiService.bindingBankCardAdvance(request);
        System.out.println("绑定银行卡推进接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.9	解绑银行卡
     */
    @Test
    public void unBindingBankCard() {
        UnbindingBankCardRequest request = new UnbindingBankCardRequest();
        request.setIdentityId("o18070922054578600001");
        request.setCardId("240337");
        request.setClientIp("127.0.0.1");
        UnbindingBankCardResponse response = sinapayApiService.unBindingBankCard(request);
        System.out.println("解绑银行卡接口响应报文："+ JSON.toJSONString(response));
    }


    /**
     *2.10	解绑银行卡推进
     */
    @Test
    public void unBindingBankCardAdvance() {
        UnbindingBankCardAdvanceRequest request = new UnbindingBankCardAdvanceRequest();
        request.setIdentityId("o18070922054578600001");
        request.setTicket("a1ffb57b193b41428608dfa715d8e84b");
        request.setValidCode("452778");
        request.setClientIp("127.0.0.1");
        UnbindingBankCardAdvanceResponse response = sinapayApiService.unBindingBankCardAdvance(request);
        System.out.println("解绑银行卡推进接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.11	查询银行卡
     */
    @Test
    public void queryBankCard() {
        QueryBankCardRequest request = new QueryBankCardRequest();
        request.setCardId("240337");
        request.setIdentityId("o18070922054578600001");
        QueryBankCardResponse response = sinapayApiService.queryBankCard(request);
        System.out.println("查询银行卡接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.12	查询余额/基金份额
     */
    @Test
    public void queryBalance() {
        QueryBalanceRequest request = new QueryBalanceRequest();
        request.setIdentityId("18090614453188960003");
        QueryBalanceResponse response = sinapayApiService.queryBalance(request);
        System.out.println("查询余额/基金份额接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.13	查询收支明细
     */
    @Test
    public void queryAccountDetails() {
        QueryAccountDetailsRequest request = new QueryAccountDetailsRequest();
        request.setIdentityId("o18070922054578600001");
        request.setStartTime("20180701000000");
        request.setEndTime(Dates.format(new Date(),"yyyyMMddHHmmss"));
        request.setPageNo(1);
        request.setPageSize(20);
        QueryAccountDetailsResponse response = sinapayApiService.queryAccountDetails(request);
        System.out.println("查询收支明细接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.14	冻结余额
     */
    @Test
    public void balanceFreeze() {
        BalanceFreezeRequest request = new BalanceFreezeRequest();
        request.setOutFreezeNo(Ids.oid());
        request.setAmount(Money.amout("1"));
        request.setIdentityId("o18070922054578600001");
        request.setClientIp("127.0.0.1");
        request.setSummary("12345678");
        BalanceFreezeResponse response = sinapayApiService.balanceFreeze(request);
        System.out.println("冻结余额接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.15	解冻余额
     */
    @Test
    public void balanceUnfreeze() {
        BalanceUnfreezeRequest request = new BalanceUnfreezeRequest();
        request.setOutUnfreezeNo(Ids.oid());
        request.setOutFreezeNo(Ids.oid());
        request.setAmount(Money.amout("1000"));
        request.setIdentityId("o18070922054578600001");
        request.setClientIp("127.0.0.1");
        request.setSummary("事件已处理");
        BalanceUnfreezeResponse response = sinapayApiService.balanceUnfreeze(request);
        System.out.println("解冻余额接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.16	查询冻结解冻结果
     */
    @Test
    public void queryCtrlResult() {
        QueryCtrlResultRequest request = new QueryCtrlResultRequest();
        request.setOutCtrlNo(Ids.oid());
        QueryCtrlResultResponse response = sinapayApiService.queryCtrlResult(request);
        System.out.println("查询冻结解冻结果接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.17	查询企业会员信息
     */
    @Test
    public void queryMemberInfos() {
        QueryMemberInfosRequest request = new QueryMemberInfosRequest();
        request.setIdentityId("o18070922054578600001");
        QueryMemberInfosResponse response = sinapayApiService.queryMemberInfos(request);
        System.out.println("查询企业会员信息接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.18	查询企业会员审核结果
     */
    @Test
    public void queryAuditResult() {
        QueryAuditResultRequest request = new QueryAuditResultRequest();
        request.setIdentityId("o18070922054578600001");
        QueryAuditResultResponse response = sinapayApiService.queryAuditResult(request);
        System.out.println("查询企业会员审核结果接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.19	请求审核企业会员资质
     */
    @Test
    public void auditMemberInfos() {
        AuditMemberInfosRequest request = new AuditMemberInfosRequest();
        request.setAuditOrderNo(Ids.oid());
        request.setIdentityId("18082010284391100002");
        request.setMemberType(SinaMemberTypeEnum.CORPORATION.getCode());
        request.setCompanyName("志客科技");
        request.setWebsite("https://www.baidu.com");
        request.setAddress("重庆市");
        request.setLicenseNo(Ids.oid());
        request.setLicenseAddress("重庆市");
        request.setLicenseExpireDate("20151002");
        request.setBusinessScope("盘活资产");
        request.setTelephone("18696725229");
        request.setEmail("317779525@qq.com");
        request.setOrganizationNo("XAIDFJAASDF");
        request.setSummary("发展潜力巨大");
        request.setLegalPerson("志客");
        request.setLegalPersonPhone("18696725229");
        request.setCertNo("500221198810192313");
        request.setBankCode("ICBC");
        request.setBankAccountNo("6217711219872134");
        request.setCardType("DEBIT");
        request.setCardAttribute("B");
        request.setProvince("重庆");
        request.setCity("重庆");
        request.setBankBranch("中国农业银行深圳南山支行");
        request.setFileName("20180918143005.zip");
        request.setDigest("aeb0361a25970708ad6145662ba88008");
//        request.setDigestType("MD5");
        request.setClientIp("101.231.34.38");
        AuditMemberInfosResponse response = sinapayApiService.auditMemberInfos(request);
        System.out.println("请求审核企业会员资质接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.20	经办人信息
     */
    @Test
    public void SmtFundAgentBuy() {
        SmtFundAgentBuyRequest request = new SmtFundAgentBuyRequest();
        request.setIdentityId("o18070922054578600001");
        request.setAgentName("志客");
        request.setAgentMobile("18696725229");
        request.setEmail("317779525@qq.com");
        request.setLicenseNo("210303198905180677");
        request.setLicenseTypeCode("ID");
        request.setClientIp("127.0.0.1");
        SmtFundAgentBuyResponse response = sinapayApiService.smtFundAgentBuy(request);
        System.out.println("经办人信息接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.21	查询经办人信息
     */
    @Test
    public void queryFundAgentBuy() {
        QueryFundAgentBuyRequest request = new QueryFundAgentBuyRequest();
        request.setIdentityId("o18070922054578600001");
        QueryFundAgentBuyResponse response = sinapayApiService.queryFundAgentBuy(request);
        System.out.println("查询经办人信息接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.22	sina页面展示用户信息
     */
    @Test
    public void showMemberInfosSina() {
        ShowMemberInfosSinaRequest request = new ShowMemberInfosSinaRequest();
        request.setIdentityId("18090614453188960003");
        ShowMemberInfosSinaResponse response = sinapayApiService.showMemberInfosSina(request);
        System.out.println("sina页面展示用户信息接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.23	修改认证手机
     *
     */
    @Test
    public void modifyVerifyMobile() {
        ModifyVerifyMobileRequest request = new ModifyVerifyMobileRequest();
        request.setReturnUrl("https://www.baidu.com");
        request.setIdentityId("o18070922054578600001");
        ModifyVerifyMobileResponse response = sinapayApiService.modifyVerifyMobile(request);
        System.out.println("修改认证手机接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.24	找回认证手机
     *
     */
    @Test
    public void findVerifyMobile() {
        FindVerifyMobileRequest request = new FindVerifyMobileRequest();
        request.setReturnUrl("https://www.baidu.com");
        request.setIdentityId("o18070922054578600001");
        FindVerifyMobileResponse response = sinapayApiService.findVerifyMobile(request);
        System.out.println("找回认证手机接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.25	修改银行预留手机
     *
     */
    @Test
    public void changeBankMobile() {
        ChangeBankMobileRequest request = new ChangeBankMobileRequest();
        request.setIdentityId("o18070922054578600001");
        request.setCardId("240337");
        request.setPhoneNo("18696725229");
        request.setRequestNo(Ids.oid());
        ChangeBankMobileResponse response = sinapayApiService.changeBankMobile(request);
        System.out.println("修改银行预留手机接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.26	修改银行预留手机推进
     *
     */
    @Test
    public void changeBankMobileAdvance() {
        ChangeBankMobileAdvanceRequest request = new ChangeBankMobileAdvanceRequest();
        request.setTicket("2b66c5065df84269a600216bc1d6f830");
        request.setValidCode("663949");
        ChangeBankMobileAdvanceResponse response = sinapayApiService.changeBankMobileAdvance(request);
        System.out.println("修改银行预留手机推进接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.27	查询交易关联号下可代付金额
     *
     */
    @Test
    public void queryTradeRelatedNo() {
        QueryTradeRelatedNoRequest request = new QueryTradeRelatedNoRequest();
        request.setTradeRelatedNo("S18082910402486100018");
        QueryTradeRelatedNoResponse response = sinapayApiService.queryTradeRelatedNo(request);
        System.out.println("查询交易关联号下可代付金额接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     *2.28	查询用户基金剩余额度
     *
     */
    @Test
    public void queryFundQuota() {
        QueryFundQuotaRequest request = new QueryFundQuotaRequest();
        request.setIdentityId("18090614453188960003");
        request.setClientIp("127.0.0.1");
        QueryFundQuotaResponse response = sinapayApiService.queryFundQuota(request);
        System.out.println("查询用户基金剩余额度接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 3.1  创建代收交易
     */
    @Test
    public void createHostingCollectTrade(){
        CreateHostingCollectTradeRequest request = new CreateHostingCollectTradeRequest();
        request.setOutTradeNo(Ids.oid());
        request.setOutTradeCode("1001");
        request.setSummary("房贷还款");
        request.setTradeCloseTime("1d");
        request.setCanRepayOnFailed("Y");
//        request.setCashdeskAddrCategory("MOBILE");
//        request.setGoodsId("320xxx");
        request.setTradeRelatedNo("o18073110463807840003");
        request.setPayerId("o18070922054578600001");
        request.setPayerIdentityType("UID");
        request.setPayerIp("192.168.66.95");
        request.setCollectTradeType("pre_auth");
        request.setAmount(Money.amout("2000"));
        request.setPayMode(SinapayMode.BALANCE);
        CreateHostingCollectTradeResponse response = sinapayApiService.createHostingCollectTrade(request);
        System.out.println("创建代收交易接口响应报文："+ JSON.toJSONString(response));

    }

    /**
     * 3.2 创建代付交易
     */
    @Test
    public void createSingleHostingPayTrade(){
        CreateSingleHostingPayTradeRequest request = new CreateSingleHostingPayTradeRequest();
        request.setOutTradeNo(Ids.oid());
        request.setOutTradeCode("2001");
        request.setPayeeIdentityId("o18070922054578600001");
        request.setAmount(Money.amout("500"));
//        List<TradePayItem> list = Lists.newArrayList();
//        TradePayItem tradePayItem = new TradePayItem("10014542", "10014542", "300", "memoxxx");
//        list.add(tradePayItem);
//        request.setSplitList(list);
        request.setSummary("房贷还款清偿");
        request.setUserIp("192.168.66.95");
        request.setTradeRelatedNo("o18073110463807840003");
//        List<DebtChangeDetail> detailList = Lists.newArrayList();
//        DebtChangeDetail debtChangeDetail = new DebtChangeDetail();
//        debtChangeDetail.setInvestorId("0254237911");
//        debtChangeDetail.setInvestorIdType("UID");
//        debtChangeDetail.setBorrowerId("19870131");
//        debtChangeDetail.setBorrowerIdType("UID");
//        debtChangeDetail.setAmount("1.00");
//        debtChangeDetail.setFundType("PRINCIPAL");
//        debtChangeDetail.setRemark("remark");
//        detailList.add(debtChangeDetail);
//        request.setCreditorInfoList(detailList);
        CreateSingleHostingPayTradeResponse response = sinapayApiService.createSingleHostingPayTrade(request);
        System.out.println("创建代付交易接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 3.3  创建批量代付交易
     */
    @Test
    public void createBatchHostingPayTrade(){
        CreateBatchHostingPayTradeRequest request = new CreateBatchHostingPayTradeRequest();
        request.setOutPayNo(Ids.oid());
        request.setOutTradeCode("2001");

        List<TradePayInfo> list = Lists.newArrayList();
        TradePayInfo tradePayInfo = new TradePayInfo();
        tradePayInfo.setOutTradeNo("20131105154925");
        tradePayInfo.setPayeeId("o18070922054578600001");
        tradePayInfo.setAmount(Money.cent(100));
        tradePayInfo.setMemo("备注");
        list.add(tradePayInfo);
        request.setTradePayInfos(list);
        request.setNotifyMethod("batch_notify");
        request.setUserIp("192.168.66.95");
        request.setExtendParam("test^true|notify_type^sync");

        CreateBatchHostingPayTradeResponse response = sinapayApiService.createBatchHostingPayTrade(request);
        System.out.println("创建批量代付交易接口响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 3.4  交易支付
     */
    @Test
    public void payHostingTrade(){
        PayHostingTradeRequest request = new PayHostingTradeRequest();
        request.setOutPayNo("20131105154925");
        request.setOuterTradeNoList("2013112405052132");
        request.setPayerIp("202.114.12.45");
        request.setAmount(Money.amout("2.11"));
        request.setExtendParam("test^true|notify_type^sync");
        PayHostingTradeResponse response = sinapayApiService.payHostingTrade(request);
        System.out.println("交易支付接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 3.5  支付结果查询
     */
    @Test
    public void queryPayResult(){
        QueryPayResultRequest request = new QueryPayResultRequest();
        request.setOutPayNo("o18073117052651840002");
        QueryPayResultResponse response = sinapayApiService.queryPayResult(request);
        System.out.println("支付结果查询接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 3.6 交易查询
     */
    @Test
    public void queryHostingTrade(){
        QueryHostingTradeRequest request = new QueryHostingTradeRequest();
        request.setIdentityId("2000011212");
        request.setIdentityType("UID");
        request.setOutTradeNo("o18073117052651840002");
//        request.setStartTime("20131117020101");
//        request.setEndTime("20131117020101");
        request.setPageNo(1);
        request.setPageSize(10);
        QueryHostingTradeResponse response = sinapayApiService.queryHostingTrade(request);
        System.out.println("交易查询接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 3.7 交易批次查询
     */
    @Test
    public void queryHostingBatchTrade(){
        QueryHostingBatchTradeRequest request = new QueryHostingBatchTradeRequest();
        request.setOutBatchNo("20131105154925");
        request.setPageNo(1);
        request.setPageSize(20);
        QueryHostingBatchTradeResponse response = sinapayApiService.queryHostingBatchTrade(request);
        System.out.println("交易批次查询接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 3.8 代收已完结的订单退款
     */
    @Test
    public void createHostingRefund(){
        CreateHostingRefundRequest request = new CreateHostingRefundRequest();
        request.setOutTradeNo(Ids.oid());
        request.setOrigOuterTradeNo("oxx011");
        request.setRefundAmount(Money.cent(200));
        request.setSummary("摘要");
        request.setUserIp("192.168.66.55");
        CreateHostingRefundResponse response = sinapayApiService.createHostingRefund(request);
        System.out.println("退款接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 3.9 退款查询
     */
    @Test
    public void queryHostingRefund(){
        QueryHostingRefundRequest request = new QueryHostingRefundRequest();
        request.setIdentityId("o18070922054578600001");
        request.setIdentityType(SinaflagType.UID.getCode());
        request.setOutTradeNo("oxx001234");
        request.setStartTime("20131117020101");
        request.setEndTime("20131117020101");
        request.setExtendParam("test^true|notify_type^sync");
        request.setPageNo(1);
        request.setPageSize(20);
        QueryHostingRefundResponse response = sinapayApiService.queryHostingRefund(request);
        System.out.println("退款查询接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 3.10 充值
     */
    @Test
    public void createHostingDeposit(){
        CreateHostingDepositRequest request = new CreateHostingDepositRequest();
        request.setOutTradeNo(Ids.oid());
        request.setSummary("账户充值");
        request.setIdentityId("o18070922054578600001");
        request.setAmount(Money.amout("4999"));
        request.setUserFee(Money.amout("1"));
        request.setPayerIp("192.168.66.55");
        request.setDepositCloseTime("15m");
//        request.setCashdeskAddrCategory("MOBILE");
        request.setPayMode(SinapayMode.ONLINE_BANK);
        request.setBankCode(SinapayBankCode.SINAPAY);
        request.setCardId("240337");
        request.setCardType(SinapayCardType.DEBIT);
        request.setCardAttribute(SinapayCardAttribute.C);
        request.setReturnUrl("https://www.baidu.com");
        CreateHostingDepositResponse response = sinapayApiService.createHostingDeposit(request);
        System.out.println("充值接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 3.11 充值查询
     */
    @Test
    public void queryHostingDeposit(){
        QueryHostingDepositRequest request = new QueryHostingDepositRequest();
        request.setIdentityId("o18070922054578600001");
        request.setAccountType(SinapayAccountType.SAVING_POT.getCode());
        request.setOutTradeNo("o18072522191443400002");
//        request.setStartTime("20131117020101");
//        request.setEndTime("20181117020101");
        request.setPageNo(1);
        request.setPageSize(20);
        QueryHostingDepositResponse response = sinapayApiService.queryHostingDeposit(request);
        System.out.println("充值查询接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 3.12 提现
     */
    @Test
    public void createHostingWithdraw(){
        CreateHostingWithdrawRequest request = new CreateHostingWithdrawRequest();
        request.setOutTradeNo(Ids.oid());
        request.setSummary("余额提现");
        request.setIdentityId("o18070922054578600001");
//        request.setAccountType(SinapayAccountType.SAVING_POT.getCode());
        request.setAmount(Money.amout("20"));
        request.setUserFee(Money.amout("1"));
//        request.setCardId("240337");
//        request.setWithdrawMode("CASHDESK");
//        request.setPaytoType("FAST");
        request.setWithdrawCloseTime("2m");
        request.setUserIp("127.0.0.1");
//        request.setCashdeskAddrCategory("MOBILE");
        request.setReturnUrl("https://www.baidu.com");
        CreateHostingWithdrawResponse response = sinapayApiService.createHostingWithdraw(request);
        System.out.println("提现接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 3.13 提现查询
     */
    @Test
    public void queryHostingWithdraw(){
        QueryHostingWithdrawRequest request = new QueryHostingWithdrawRequest();
        request.setIdentityId("o18070922054578600001");
        request.setAccountType(SinapayAccountType.SAVING_POT.getCode());
        request.setOutTradeNo("o18080111031896360002");
//        request.setStartTime("20131117020101");
//        request.setEndTime("20131117020101");
//        request.setPageNo(1);
//        request.setPageSize(20);
        QueryHostingWithdrawResponse response = sinapayApiService.queryHostingWithdraw(request);
        System.out.println("提现查询接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 3.14 创建单笔代付到提现卡交易
     */
    @Test
    public void createSingleHostingPayToCardTrade(){
        CreateSingleHostingPayToCardTradeRequest request = new CreateSingleHostingPayToCardTradeRequest("房贷还款清偿","o18070922054578600001","192.168.55.66", Money.cent(300), "6220");
        request.setOutTradeNo("20131105154925");
        request.setOutTradeCode("2001");
        request.setCollectMethod("binding_card^10014543,UID,5446010");
        request.setPaytoType("GENERAL");
        request.setGoodsId("o001xx");
        request.setTradeRelatedNo("10002");

        List<DebtChangeDetail> list = Lists.newArrayList();
        DebtChangeDetail debtChangeDetail = new DebtChangeDetail();
        debtChangeDetail.setInvestorId("0254237911");
        debtChangeDetail.setInvestorIdType("UID");
        debtChangeDetail.setBorrowerId("19870131");
        debtChangeDetail.setBorrowerIdType("UID");
        debtChangeDetail.setAmount("1.00");
        debtChangeDetail.setFundType("PRINCIPAL");
        debtChangeDetail.setRemark("remark");

        list.add(debtChangeDetail);
        request.setCreditorInfoList(list);

        CreateSingleHostingPayToCardTradeResponse response = sinapayApiService.createSingleHostingPayToCardTrade(request);
        System.out.println("创建单笔代付到提现卡交易接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 3.15 创建批量代付到提现卡交易
     */
    @Test
    public void createBatchHostingPayToCardTrade(){
        CreateBatchHostingPayToCardTradeRequest request = new CreateBatchHostingPayToCardTradeRequest();
        request.setOutPayNo("20131105154925");
        request.setOutTradeCode("2001");
        request.setTradeList("20131105154925~ binding_card^10014543,UID,5446010~300.00~房贷还款清偿~$20131105154926~binding_card^10014543,UID,5446010~300.00~房贷还款清偿~");
        request.setNotifyMethod(SinaNotifyMethod.SINGLE_NOTIFY.getCode());
        request.setPaytoType(SinaPaytoType.GENERAL.getCode());
        request.setUserIp("202.114.12.45");
        request.setExtendParam("test^true|notify_type^sync");
        CreateBatchHostingPayToCardTradeResponse response = sinapayApiService.createBatchHostingPayToCardTrade(request);
        System.out.println("创建批量代付到提现卡交易接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 3.16	代收完成
     */
    @Test
    public void finishPreAuthTrade(){
        FinishPreAuthTradeRequest request = new FinishPreAuthTradeRequest();
        request.setOutRequestNo(Ids.oid());
        request.setUserIp("202.114.12.45");
        List<TradeOptInfo> tradeOptInfos = Lists.newArrayList();
        TradeOptInfo tradeOptInfo = new TradeOptInfo();
        tradeOptInfo.setAmount(Money.amout("1000").toString());
        tradeOptInfo.setOutTradeNo("o18073117265251320002");
        tradeOptInfo.setSubOutTradeNo(Ids.oid());
        tradeOptInfo.setMemo("满标扣款");
        tradeOptInfos.add(tradeOptInfo);
        request.setTradeOptInfos(tradeOptInfos);
        FinishPreAuthTradeResponse response = sinapayApiService.finishPreAuthTrade(request);
        System.out.println("代收完成接口响应报文：" + JSON.toJSONString(response));
    }



    /**
     * 委托扣款重定向
     */
    @Test
    public void handleWithholdAuthority(){
        HandleWithholdAuthorityRequest request = new HandleWithholdAuthorityRequest();
        request.setIdentityId("o18070922054578600001");
        request.setReturnUrl("https://www.baidu.com");
        HandleWithholdAuthorityResponse response = sinapayApiService.handleWithholdAuthority(request);
        System.out.println("委托扣款重定向接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 查询委托扣款
     */
    @Test
    public void handleWithholdAuthorityQuery(){
        QueryWithholdAuthorityRequest request = new QueryWithholdAuthorityRequest();
        request.setIdentityId("o18070922054578600001");
        request.setReturnUrl("https://www.baidu.com");
        QueryWithholdAuthorityResponse response = sinapayApiService.queryWithholdAuthority(request);
        System.out.println("查询委托扣款接口响应报文：" + JSON.toJSONString(response));
    }
}
