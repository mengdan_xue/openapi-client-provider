package com.acooly.module.openapi.client.provider.yipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayApiMsg;
import com.acooly.module.openapi.client.provider.yipay.domain.YipayResponse;
import com.acooly.module.openapi.client.provider.yipay.enums.YipayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/4/18 19:02
 */
@Getter
@Setter
@YipayApiMsg(service = YipayServiceNameEnum.DEDUCT,type = ApiMessageType.Response)
public class YipayDeductResponse extends YipayResponse {

    /**
     * 翼支付订单号（对账单号））
     * success为true成功时不为空，等于结果通知参数的coreOrderCode
     */
    private String result;
}
