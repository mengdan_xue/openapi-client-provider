/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-17 17:49 创建
 */
package com.acooly.module.openapi.client.provider.shengpay;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayApiMsg;

/**
 * @author zhike 2017-09-17 17:49
 */
public class ShengpayConstants {

    /**
     * 提供方
     */
    public static final String PROVIDER_NAME = "shengpay";
    public static final String PROVIDER_DEF_PRINCIPAL = "principal";


    public static final String SHENGPAY_CHARSET = "UTF-8";

    public static final String SHENGPAY_SIGN_TYPE = "RSA";
    public static final String SHENGPAY_MD5_SIGN_TYPE = "MD5";


    /**
     * 核心报文和协议字段
     */
    public static final String REQUEST_BODY_KEY = "requestBody";
    public static final String SIGN_TYPE_KEY = "signType";
    public static final String SIGN_MSG_KEY = "signMsg";
    public static final String SIGN = "sign";

    public static final String RESPONSE_BODY_KEY = "responseBody";

    public static final String REQUEST_PARAM_NAME = "notifyData";
    public static final String NOTIFY_DATA_JSON = "notifyDataJson";
    public static final String NOTIFY_SUB_URL = "/gateway/notify/shengpayNotify/";



    public static String getCanonicalUrl(String gatewayUrl, String serviceName) {
        String serviceUrl = gatewayUrl;
        serviceUrl = Strings.removeEnd(serviceUrl, "/");
        if (!Strings.startsWith(serviceName, "/")) {
            serviceUrl = serviceUrl + "/" + serviceName;
        } else {
            serviceUrl = serviceUrl + serviceName;
        }
        return serviceUrl;
    }

    public static String getServiceName(ApiMessage apiMessage) {
        if (Strings.isNotBlank(apiMessage.getService())) {
            return apiMessage.getService();
        }

        ShengpayApiMsg apiMsgInfo = getApiMsgInfo(apiMessage);
        if (apiMsgInfo != null && apiMsgInfo.service() != null) {
            return apiMsgInfo.service().code();
        }
        throw new RuntimeException("请求报文的service为空");
    }


    public static ShengpayApiMsg getApiMsgInfo(ApiMessage apiMessage) {
        return apiMessage.getClass().getAnnotation(ShengpayApiMsg.class);
    }

}
