package com.acooly.module.openapi.client.provider.hx.message.xStream.netBankPay.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

/**
 * @author fufeng 2018/3/2 17:51.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("ReqNetBankBody")
public class ReqNetBankBody {

    /**
     *商户订单号
     * 000001000123
     *格式：字母及数字
     */
    @XStreamAlias("MerBillNo")
    private String merBillNo;

    /**
     *支付方式
     * 01#借记卡
     * 默认 01
     */
    @XStreamAlias("Amount")
    private String amount;

    /**
     *订单日期
     *规则：yyyyMMdd
     *20031205
     */
    @XStreamAlias("Date")
    private String date;

    /**
     *币种
     *156#人民币
     */
    @XStreamAlias("CurrencyType")
    private String currencyType;

    /**
     *订单金额
     * 保留 2 位小数
     * 格式：12.00
     */
    @XStreamAlias("GatewayType")
    private String gatewayType;

    /**
     *语言
     * 规则：GB——GB 中文（缺省）
     * GB
     */
    @XStreamAlias("Lang")
    private String lang;

    /**
     *Merchanturl
     * 支付结果成功返回的商户 URL
     *规则：动态的网页，在该页对IPS 返回信息进行签名验证后处理商户端的数据库
     */
    @XStreamAlias("Merchanturl")
    private String merchanturl;

    /**
     *支付结果失败返回的商户 URL
     * 规则：动态的网页，在该页对
     *IPS 返回信息进行签名验证后
     *处理商户端的数据库。
     *缺省：
     */
    @XStreamAlias("FailUrl")
    private String failUrl;

    /**
     *商户数据包
     * 存放商户自己的信息，随订单
     *传送到 IPS 平台，当订单返回
     *的时候原封不动的返回给商
     *户，由“数字、字母或数字+字
     *母”组成
     */
    @XStreamAlias("Attach")
    private String attach;

    /**
     *订单支付接口加密方式
     *说明：存放商户所选择订单支付接口加密方式。
     *5#订单支付采用 Md5 的摘要认证方式
     */
    @XStreamAlias("OrderEncodeType")
    private String orderEncodeType;

    /**
     *交易返回接口加密方式
     * 说明：存放商户所选择的交易返回接口加密方式。
     *17#交易返回采用 Md5 的摘要认证方式
     */
    @XStreamAlias("RetEncodeType")
    private String retEncodeType;

    /**
     *返回方式
     * Server to Server 返回。1#S2S 返回 RetType=1
     */
    @XStreamAlias("RetType")
    private String RetType;

    /**
     *异步S2S返回
     * 商户使用异步方式返回时可将返回地址存于此字段 当 RetType=1 时,本字段有效
     */
    @XStreamAlias("ServerUrl")
    private String serverUrl;

    /**
     *订单有效期
     * 订单有效期(以小时计算，必须是整数)
     *过了订单的有效时间 IPS 没处理完，订单将自动过期做失败处理
     */
    @XStreamAlias("BillEXP")
    private String billEXP;

    /**
     *商品名称
     * 用户购买的商品的名称
     */
    @XStreamAlias("GoodsName")
    private String goodsName;

    /**
     *直连选项
     * 决定商户是否参用直连方式
     * 1 #直连必填
     */
    @XStreamAlias("IsCredit")
    private String isCredit;

    /**
     *银行号
     * IPS 唯一标识指定支付银行的编号
     * 编号1100直连必填
     */
    @XStreamAlias("BankCode")
    private String bankCode;

    /**
     *产品类型
     * 1#个人网银 2#企业网银
     * 直连必填
     */
    @XStreamAlias("ProductType")
    private String productType;

    /**
     *姓名
     */
    @XStreamAlias("UserRealName")
    private String userRealName;

    /**
     *平台用户名
     */
    @XStreamAlias("UserId")
    private String userId;

    /**
     *持卡人信息
     * 1， 当需要传输时请按此规则提交。
     *2， 3DES 加密字段
     *3， 暂只支持 H5 网关
     */
    @XStreamAlias("CardInfo")
    private String cardInfo;

}
