/** create by zhike date:2015年3月11日 */
package com.acooly.module.openapi.client.provider.allinpay.domain;

import lombok.Getter;
import lombok.Setter;

/** @author zhike */
@Getter
@Setter
public class AllinpayNotify extends AllinpayResponse {

}
