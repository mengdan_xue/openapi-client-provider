/**
 * create by zhike
 * date:2015年3月2日
 */
package com.acooly.module.openapi.client.provider.allinpay;

import com.acooly.core.utils.Strings;
import com.acooly.core.utils.net.HttpResult;
import com.acooly.module.openapi.client.api.AbstractApiServiceClient;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.exception.ApiClientProcessingException;
import com.acooly.module.openapi.client.api.exception.ApiClientSocketTimeoutException;
import com.acooly.module.openapi.client.api.exception.ApiServerException;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayApiMsgInfo;
import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayNotify;
import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayRequest;
import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayResponse;
import com.acooly.module.openapi.client.provider.allinpay.marshall.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * ApiService执行器
 *
 * @author zhike
 */
@Component("allinpayApiServiceClient")
@Slf4j
public class AllinpayApiServiceClient
        extends AbstractApiServiceClient<AllinpayRequest, AllinpayResponse, AllinpayNotify, AllinpayNotify> {


    public static final String PROVIDER_NAME = "allinpay";

    @Resource(name = "allinpayHttpTransport")
    private Transport transport;

    @Resource(name = "allinpayRequestMarshall")
    private AllinpayRequestMarshall requestMarshal;

    @Resource(name = "allinpayRedirectMarshall")
    private AllinpayRedirectMarshall redirectMarshal;

    @Resource(name = "allinpaySignMarshall")
    private AllinpaySignMarshall signMarshall;

    @Resource(name = "allinpayRedirectPostMarshall")
    private AllinpayRedirectPostMarshall allinpayRedirectPostMarshall;

    @Autowired
    private AllinpayResponseUnmarshall responseUnmarshal;
    @Autowired
    private AllinpayNotifyUnmarshall notifyUnmarshal;

    @Autowired
    private OpenAPIClientAllinpayProperties openAPIClientAllinpayProperties;

    @Override
    public AllinpayResponse execute(AllinpayRequest request) {
        try {
            beforeExecute(request);
            //组装请求报文头
            String url = request.getGatewayUrl();
            String requestMessage = getRequestMarshal().marshal(request);
            log.info("请求报文: {}", requestMessage);
            HttpResult result = getTransport().request(requestMessage, url);
            if (result.getStatus() >= HttpStatus.SC_BAD_REQUEST) {
                throw new RuntimeException("HttpStatus:" + result.getStatus());
            }
            if (Strings.isBlank(result.getBody())) {
                throw new ApiClientException("响应报文为空");
            }
            AllinpayResponse t = getResponseUnmarshal().unmarshal(result.getBody(), request.getService());
            afterExecute(t);
            return t;
        } catch (ApiClientProcessingException pe) {
            log.error("解析响应报文异常：" + pe.getMessage(), pe);
            throw pe;
        } catch (ApiClientSocketTimeoutException ase) {
            log.error("响应超时异常：" + ase.getMessage(), ase);
            throw new ApiClientProcessingException(ase.getMessage());
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    public AllinpayNotify notice(HttpServletRequest request, String serviceKey) {
        try {
            Map<String, String> notifyData = getSignMarshall().getDateMap(request);
            AllinpayNotify notify = getNoticeUnmarshal().unmarshal(notifyData, serviceKey);
            afterNotice(notify);
            return notify;
        } catch (ApiClientException oce) {
            log.warn("客户端:{}", oce.getMessage());
            throw oce;
        } catch (Exception e) {
            log.warn("内部错误:{}", e.getMessage());
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    @Override
    public PostRedirect redirectPost(AllinpayRequest request) {
        try {
            beforeExecute(request);
            PostRedirect postRedirect = allinpayRedirectPostMarshall.marshal(request);
            return postRedirect;
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    @Override
    protected ApiMarshal<String, AllinpayRequest> getRequestMarshal() {
        return this.requestMarshal;
    }

    @Override
    protected ApiUnmarshal<AllinpayResponse, String> getResponseUnmarshal() {
        return this.responseUnmarshal;
    }

    @Override
    protected ApiUnmarshal<AllinpayNotify, Map<String, String>> getNoticeUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    protected ApiMarshal<String, AllinpayRequest> getRedirectMarshal() {
        return this.redirectMarshal;
    }

    @Override
    protected Transport getTransport() {
        return this.transport;
    }

    @Override
    protected ApiUnmarshal<AllinpayNotify, Map<String, String>> getReturnUnmarshal() {
        return this.notifyUnmarshal;
    }

    public AllinpaySignMarshall getSignMarshall() {
        return signMarshall;
    }

    public void setSignMarshall(AllinpaySignMarshall signMarshall) {
        this.signMarshall = signMarshall;
    }

    @Override
    public String getName() {
        return PROVIDER_NAME;
    }

    @Override
    protected void beforeExecute(AllinpayRequest request) {
        AllinpayApiMsgInfo apiMsgInfo = request.getClass().getAnnotation(AllinpayApiMsgInfo.class);
        request.setService(apiMsgInfo.service().getCode());
        if (Strings.isBlank(request.getGatewayUrl())) {
            request.setGatewayUrl(openAPIClientAllinpayProperties.getGatewayUrl());
        }
        MDC.put("service", request.getService());
    }
}
