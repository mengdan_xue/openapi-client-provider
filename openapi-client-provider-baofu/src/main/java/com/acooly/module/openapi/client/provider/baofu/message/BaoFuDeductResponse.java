package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuResponse;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/1/26 13:56
 */
@Getter
@Setter
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.DEDUCT,type = ApiMessageType.Response)
@XStreamAlias("result")
public class BaoFuDeductResponse extends BaoFuResponse{

    /**
     * 请求方保留域
     */
    @XStreamAlias("req_reserved")
    private String reqReserved;

    /**
     * 附加字段
     */
    @XStreamAlias("additional_info")
    private String additionalInfo;

    /**
     * 接入类型 默认0000,表示为储蓄卡支付。
     */
    @XStreamAlias("biz_type")
    private String bizType;

    /**
     * 成功金额 单位：分
     */
    @XStreamAlias("succ_amt")
    private String succAmt;

    /**
     * 商户流水号 8-20 位字母和数字，每次请求都不可重复
     *
     */
    @XStreamAlias("trans_serial_no")
    private String transSerialNo;
}
