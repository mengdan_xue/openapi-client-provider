package com.acooly.module.openapi.client.provider.baofu.message.info;

import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuDataTypeEnum;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceVersionEnum;
import com.acooly.module.openapi.client.provider.baofu.support.BaoFuAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 宝付提现最终提交请求的参数实体
 */
@Getter
@Setter
public class BaoFuRequestParams implements Serializable{

    /**
     *商户号
     */
    @BaoFuAlias(value = "member_id")
    private String memberId;

    /**
     *终端号
     */
    @BaoFuAlias(value = "terminal_id")
    private String terminalId;

    /**
     * xml/json
     */
    @BaoFuAlias(value = "data_type")
    private String dataType = BaoFuDataTypeEnum.xml.getCode();

    /**
     * 加密数据串
     */
    @BaoFuAlias(value = "data_content")
    private String dataContent;

    /**
     * 版本
     */
    @BaoFuAlias(value = "version")
    private String version = BaoFuServiceVersionEnum.VERSION_400.getCode();
}
