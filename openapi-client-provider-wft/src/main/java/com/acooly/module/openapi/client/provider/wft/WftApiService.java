/**
 * create by zhangpu
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.wft;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.provider.wft.domain.WftNotify;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.message.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author zhangpu
 */
@Service
public class WftApiService {

    @Resource(name = "wftApiServiceClient")
    private WftApiServiceClient apiServiceClient;

    @Autowired
    private OpenAPIClientWftProperties openAPIClientWftProperties;

    /**
     * 微信支付宝被扫
     *
     * @param request
     * @return
     */
    public WftUnifiedTradeMicropayResponse unifiedTradeMicropay(WftUnifiedTradeMicropayRequest request) {
        request.setService(WftServiceEnum.UNIFIED_TRADE_MICROPAY.getCode());
        return (WftUnifiedTradeMicropayResponse) apiServiceClient.execute(request);
    }

    /**
     * 订单撤销
     *
     * @param request
     * @return
     */
    public WftUnifiedMicropayReverseResponse unifiedMicropayReverse(WftUnifiedMicropayReverseRequest request) {
        request.setService(WftServiceEnum.UNIFIED_MICROPAY_REVERSE.getCode());
        return (WftUnifiedMicropayReverseResponse) apiServiceClient.execute(request);
    }

    /**
     * 订单退款
     *
     * @param request
     * @return
     */
    public WftUnifiedTradeRefundResponse unifiedTradeRefund(WftUnifiedTradeRefundRequest request) {
        request.setService(WftServiceEnum.UNIFIED_TRADE_REFUND.getCode());
        request.setOpUserId(openAPIClientWftProperties.getPartnerId());
        return (WftUnifiedTradeRefundResponse) apiServiceClient.execute(request);
    }


    /**
     * 退款订单查询
     *
     * @param request
     * @return
     */
    public WftUnifiedTradeRefundQueryResponse unifiedTradeRefundQuery(WftUnifiedTradeRefundQueryRequest request) {
        request.setService(WftServiceEnum.UNIFIED_TRADE_REFUNDQUERY.getCode());
        return (WftUnifiedTradeRefundQueryResponse) apiServiceClient.execute(request);
    }

    /**
     * 微信主扫
     *
     * @param request
     * @return
     */
    public WftPayWeixinNativeResponse payWeixinNative(WftPayWeixinNativeRequest request) {
        request.setService(WftServiceEnum.PAY_WEIXIN_NATIVE.getCode());
        request.setNotifyUrl(openAPIClientWftProperties.getDomain() + "/gateway/notify/wftNotify/" + WftServiceEnum.PAY_WEIXIN_NATIVE.getKey());
        return (WftPayWeixinNativeResponse) apiServiceClient.execute(request);
    }

    /**
     * 订单关闭
     *
     * @param request
     * @return
     */
    public WftUnifiedTradeCloseResponse unifiedTradeClose(WftUnifiedTradeCloseRequest request) {
        request.setService(WftServiceEnum.UNIFIED_TRADE_CLOSE.getCode());
        return (WftUnifiedTradeCloseResponse) apiServiceClient.execute(request);
    }

    /**
     * 支付宝主扫
     *
     * @param request
     * @return
     */
    public WftPayAlipayNativeResponse payAlipayNative(WftPayAlipayNativeRequest request) {
        request.setService(WftServiceEnum.PAY_ALIPAY_NATIVE.getCode());
        request.setNotifyUrl(openAPIClientWftProperties.getDomain() + "/gateway/notify/wftNotify/" + WftServiceEnum.PAY_ALIPAY_NATIVE.getKey());
        return (WftPayAlipayNativeResponse) apiServiceClient.execute(request);
    }

    /**
     * 微信公众号支付
     *
     * @param request
     * @return
     */
    public WftPayWeixinJspayResponse payWeixinJspay(WftPayWeixinJspayRequest request) {
        request.setService(WftServiceEnum.PAY_WEIXIN_JSPAY.getCode());
        request.setNotifyUrl(openAPIClientWftProperties.getDomain() + "/gateway/notify/wftNotify/" + WftServiceEnum.PAY_WEIXIN_JSPAY.getKey());
        return (WftPayWeixinJspayResponse) apiServiceClient.execute(request);
    }

    /**
     * 微信APP支付
     *
     * @param request
     * @return
     */
    public WftPayWeixinRawAppResponse payWeixinRawApp(WftPayWeixinRawAppRequest request) {
        request.setService(WftServiceEnum.PAY_WEIXIN_RAW_APP.getCode());
        request.setNotifyUrl(openAPIClientWftProperties.getDomain() + "/gateway/notify/wftNotify/" + WftServiceEnum.PAY_WEIXIN_RAW_APP.getKey());
        return (WftPayWeixinRawAppResponse) apiServiceClient.execute(request);
    }

    /**
     * 订单查询
     *
     * @param request
     * @return
     */
    public WftUnifiedTradeQueryResponse unifiedTradeQuery(WftUnifiedTradeQueryRequest request) {
        request.setService(WftServiceEnum.TRADE_ORDER_QUERY.getCode());
        return (WftUnifiedTradeQueryResponse) apiServiceClient.execute(request);
    }

    /**
     * 对账文件下载
     *
     * @param request
     * @return
     */
    public WftBillDownloadResponse billDownload(WftBillDownloadRequest request) {
        if (Strings.isBlank(request.getService())) {
            request.setService(WftServiceEnum.BILL_DOWNLOAD.getCode());
        }
        return apiServiceClient.billDownload(request);
    }

    /**
     * 解析异步通知
     *
     * @param request
     * @param serviceKey
     * @return
     */
    public WftNotify notice(HttpServletRequest request, String serviceKey) {
        return apiServiceClient.notice(request, serviceKey);
    }

    /**
     * 签名
     *
     * @param waitSignMap
     * @param partnerId
     * @return
     */
    public String sign(Map<String, String> waitSignMap, String partnerId) {
        return apiServiceClient.getSignMarshall().sign(waitSignMap, partnerId);
    }

    /**
     * 验签
     *
     * @param request
     * @return
     */
    public boolean verySign(HttpServletRequest request, String partnerId) {
        return apiServiceClient.getSignMarshall().verySign(request, partnerId);
    }


    /**
     * 验签
     *
     * @param responseData
     * @return
     */
    public boolean verySign(Map<String, String> responseData, String partnerId) {
        return apiServiceClient.getSignMarshall().verySign(responseData, partnerId);
    }
}
