/**
 * create by zhangpu
 * date:2015年3月2日
 */
package com.acooly.module.openapi.client.provider.cmb;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.AbstractApiServiceClient;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.exception.ApiServerException;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.cmb.domain.CmbNotify;
import com.acooly.module.openapi.client.provider.cmb.domain.CmbRequest;
import com.acooly.module.openapi.client.provider.cmb.domain.CmbResponse;
import com.acooly.module.openapi.client.provider.cmb.marshall.CmbNotifyUnmarshall;
import com.acooly.module.openapi.client.provider.cmb.marshall.CmbRedirectMarshall;
import com.acooly.module.openapi.client.provider.cmb.marshall.CmbRedirectPostMarshall;
import com.acooly.module.openapi.client.provider.cmb.marshall.CmbRequestMarshall;
import com.acooly.module.openapi.client.provider.cmb.marshall.CmbResponseUnmarshall;
import com.acooly.module.openapi.client.provider.cmb.marshall.CmbSignMarshall;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;

/**
 * 上海银行P2P存管ApiService执行器  外部业务禁止使用
 *
 * @author zhangpu
 */
@Slf4j
@Component("cmbApiServiceClient")
public class CmbApiServiceClient
        extends AbstractApiServiceClient<CmbRequest, CmbResponse, CmbNotify, CmbNotify> {

    public static final String PROVIDER_NAME = "cmb";

    @Resource(name = "cmbHttpTransport")
    private Transport transport;
    @Resource(name = "cmbRequestMarshall")
    private CmbRequestMarshall requestMarshal;
    @Resource(name = "cmbRedirectMarshall")
    private CmbRedirectMarshall redirectMarshal;
    @Resource(name = "cmbRedirectPostMarshall")
    private CmbRedirectPostMarshall cmbRedirectPostMarshall;
    @Resource(name = "cmbSignMarshall")
    private CmbSignMarshall signMarshall;

    @Autowired
    private CmbResponseUnmarshall responseUnmarshal;

    @Autowired
    private CmbNotifyUnmarshall notifyUnmarshal;

    @Autowired
    private CmbProperties cmbProperties;

    @Override
    public CmbResponse execute(CmbRequest request) {
        try {
            beforeExecute(request);
            if(Strings.isBlank(request.getGatewayUrl())) {
                request.setGatewayUrl(cmbProperties.getGatewayUrl());
            }
            String url = request.getGatewayUrl();
            String requestMessage = getRequestMarshal().marshal(request);
            log.info("请求报文: {}", requestMessage);
            String responseMessage = getTransport().exchange(requestMessage, url);
            CmbResponse t = getResponseUnmarshal().unmarshal(responseMessage, request.getService());
            afterExecute(t);
            return t;
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    public CmbNotify notice(HttpServletRequest request, String serviceKey) {
        try {
            Map<String,String> notifyData = getSignMarshall().getDateMap(request);
            CmbNotify notify = getNoticeUnmarshal().unmarshal(notifyData, serviceKey);
            afterNotice(notify);
            return notify;
        } catch (ApiClientException oce) {
            log.warn("客户端:{}", oce.getMessage());
            throw oce;
        } catch (Exception e) {
            log.warn("内部错误:{}", e.getMessage());
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    public CmbSignMarshall getSignMarshall() {
        return signMarshall;
    }

    @Override
    public PostRedirect redirectPost(CmbRequest request) {
        try {
            beforeExecute(request);
            PostRedirect postRedirect = cmbRedirectPostMarshall.marshal(request);
            return postRedirect;
        } catch (ApiServerException ose) {
            log.error("服务器:" + ose.getMessage(), ose);
            throw ose;
        } catch (ApiClientException oce) {
            log.error("客户端:" + oce.getMessage(), oce);
            throw oce;
        } catch (Exception e) {
            log.error("内部错误:" + e.getMessage(), e);
            throw new ApiClientException("内部错误:" + e.getMessage());
        }
    }

    @Override
    protected ApiMarshal<String, CmbRequest> getRequestMarshal() {
        return this.requestMarshal;
    }

    @Override
    protected ApiUnmarshal<CmbResponse, String> getResponseUnmarshal() {
        return this.responseUnmarshal;
    }

    @Override
    protected ApiUnmarshal<CmbNotify, Map<String, String>> getNoticeUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    protected ApiMarshal<String, CmbRequest> getRedirectMarshal() {
        return this.redirectMarshal;
    }

    @Override
    protected Transport getTransport() {
        return this.transport;
    }

    @Override
    protected ApiUnmarshal<CmbNotify, Map<String, String>> getReturnUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    public String getName() {
        return PROVIDER_NAME;
    }


}
