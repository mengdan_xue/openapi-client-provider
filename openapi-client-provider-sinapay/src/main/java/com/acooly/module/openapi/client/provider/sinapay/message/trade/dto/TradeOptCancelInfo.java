/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月13日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade.dto;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ApiDto;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ItemOrder;
import com.acooly.module.openapi.client.provider.sinapay.message.Dtoable;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 交易操作DTO
 * 
 * 用于：代收撤销
 * <li>代收撤销：取消投资，解冻资金</li>
 * 
 * @author zhike
 */
@Getter
@Setter
@ApiDto(colSeparator = '~', rowSeparator = '$')
public class TradeOptCancelInfo implements Dtoable {

	/*
	 * 子交易订单号（建议为主订单号顺序加一位顺序计数）
	 *
	 * 商户网站交易订单号，商户内部保证唯一
	 */
	@NotEmpty
	@Size(max = 32)
	@ItemOrder(0)
	private String subOutTradeNo = Ids.oid();

	/*
	 * 操作目标单号。冻结单号
	 */
	@NotEmpty
	@Size(max = 32)
	@ItemOrder(1)
	private String outTradeNo;

	/** 摘要 */
	@NotEmpty
	@Size(max = 64)
	@ItemOrder(3)
	private String memo;

	@Size(max = 200)
	@ItemOrder(4)
	private String extendParam;

	public TradeOptCancelInfo() {
		super();
	}

	/**
	 * @param outTradeNo
	 * @param memo
	 */
	public TradeOptCancelInfo(String outTradeNo, String memo) {
		super();
		this.outTradeNo = outTradeNo;
		this.memo = memo;
	}

}
