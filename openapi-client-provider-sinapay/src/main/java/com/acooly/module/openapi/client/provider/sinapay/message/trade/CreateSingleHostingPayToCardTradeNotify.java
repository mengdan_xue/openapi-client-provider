/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月6日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.CREATE_SINGLE_HOSTING_PAY_TO_CARD_TRADE, type = ApiMessageType.Notify)
public class CreateSingleHostingPayToCardTradeNotify extends TradeNotify {

	
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "withdraw_status")
	private String withdrawStatus;
	
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "withdraw_amount")
	private String withdrawAmount;
}
