/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2018-01-23 12:58 创建
 */
package com.acooly.module.openapi.client.provider.sinapay.domain;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018-01-23 12:58
 * <p>
 * 如果是响应错误，则content都是固定的格式:
 * "merchantNo": "M0980033473",
 * "orderDate": "20171025",
 * "orderNo": "20171025000004923708"
 */
@Getter
@Setter
public class SinapayResponse extends SinapayMessage {

    @ApiItem
    private String service;

    /** 响应时间 yyyyMMddHHmmss */
    @ApiItem(value = "reaponse_time")
    private String responseTime;

    @ApiItem("_input_charset")
    private String inputCharset;

    @ApiItem(value = "sign", sign = false)
    private String sign;

    @ApiItem(value = "sign_type", sign = false)
    private String signType = "RSA";

    @ApiItem(value = "sign_version", sign = false)
    private String signVersion;

    @ApiItem("response_code")
    private String responseCode;

    @ApiItem("response_message")
    private String responseMessage;

    private String memo;
}
